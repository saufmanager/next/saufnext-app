FROM openjdk:19-alpine AS builder
COPY . /src
WORKDIR /src
RUN ./gradlew fullJar

FROM openjdk:19-alpine
COPY --from=builder /src/build/libs/saufnext-app.jar /app/server.jar
WORKDIR /app
ENTRYPOINT ["java", "-jar", "/app/server.jar"]
