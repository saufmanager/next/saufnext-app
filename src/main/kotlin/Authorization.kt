import io.ktor.http.*
import io.ktor.http.auth.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.response.*
import lib.extensions.cookieName
import models.User
import org.jetbrains.exposed.sql.transactions.transaction

object Authorization {

    fun AuthenticationConfig.setup() {
        jwt {
            authHeader {
                it.request.cookies["Access-Token".cookieName]?.let { token ->
                    HttpAuthHeader.Single("Bearer", token)
                }
            }
            verifier(accessTokenVerifier)
            validate { credential -> transaction { User.byEmail(credential.payload.subject) } }
            challenge { _, _ -> call.respond(HttpStatusCode.Unauthorized) }
        }
    }
}
