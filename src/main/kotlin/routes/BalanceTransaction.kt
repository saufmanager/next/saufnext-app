package routes

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.serialization.Serializable
import lib.extensions.receiveAndRespond
import lib.extensions.requirePermissions
import lib.extensions.usePrincipal
import models.*
import models.BalanceTransactions.amount
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDateTime
import kotlin.reflect.jvm.internal.impl.descriptors.Visibilities.Local

@Serializable
data class CashBalance(val balance: Int)

fun Route.balanceTransactionRoute() = route("/balance") {
    get {
        call.requirePermissions(Permission.ManageCash) ?: return@get call.respond(HttpStatusCode.Forbidden)
        call.respond(CashBalance(
            transaction {
                BalanceTransaction.all().sumOf { it.calculationAmount } +
                        Transaction.all().sumOf { it.calculationAmount }
            }
        ))
    }
    get("transactions") {
        call.requirePermissions(Permission.ManageCash) ?: return@get call.respond(HttpStatusCode.Forbidden)
        call.respond(transaction {
            BalanceTransaction.all().map { it.data }
        })
    }
    put("transaction") {
        call.requirePermissions(Permission.ManageCash) ?: return@put call.respond(HttpStatusCode.Forbidden)
        call.receiveAndRespond { tx: BalanceTransactionData ->
            transaction {
                BalanceTransaction.new {
                    type = tx.type
                    description = tx.description
                    amount = tx.amount
                    createdAt = LocalDateTime.now()
                }.data
            }
        }
    }
}
