package routes

import Auth
import Config
import createAccessToken
import createExchangeToken
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import issuer
import jwtVerification
import kotlinx.serialization.Serializable
import lib.extensions.*
import models.User
import models.Users
import namedLogger
import org.jetbrains.exposed.sql.transactions.transaction
import java.security.SecureRandom
import java.time.temporal.ChronoUnit

@Serializable
data class LoginRequest(
    val email: String,
)

@Serializable
data class LoginResponse(
    val token: String = "",
    val error: LoginError? = null,
)

enum class LoginError {
    EmailInvalid, EmailNotAllowed;

    val response get() = LoginResponse(error = this)
}

@Serializable
data class LoginConfirmation(
    val exchangeToken: String,
    val code: String,
)

@Serializable
data class LoginConfirmationResponse(
    val error: LoginConfirmationError? = null
)

enum class LoginConfirmationError {
    AuthCodeInvalid;

    val response get() = LoginConfirmationResponse(this)
}

private val random = SecureRandom()

private const val csrfCookie = "csrf"

private val logger by namedLogger("LoginRoute")

fun Routing.loginRoute() = route("/login") {
    post {
        call.receive { loginRequest: LoginRequest ->
            logger.info("Login requested: $loginRequest")
            if (!loginRequest.email.matches(Regex(".+@.+[.].+"))) {
                logger.info("Invalid email")
                return@post call.respond(HttpStatusCode.BadRequest, LoginError.EmailInvalid.response)
            }
            if (!Config.userEmailAllowed(loginRequest.email)) {
                logger.info("User email domain violation: ${loginRequest.email}")
                return@post call.respond(HttpStatusCode.Forbidden, LoginError.EmailNotAllowed.response)
            }
            logger.info("Sending auth code")
            User.byEmailOrCreate(loginRequest.email) { authCode = Auth.sendCodeEmail(loginRequest.email) }
            val csrfCode = random.nextBits(256)
            val token = loginRequest.createExchangeToken(csrfCode)
            call.addCookie(csrfCookie, csrfCode.hex, now.plusMinutes(20))
            call.expireCookie("Access-Token")
            logger.info("Login allowed!")
            call.respond(LoginResponse(token))
        }
    }
    post("/confirm") {
        call.receive { confirmation: LoginConfirmation ->
            val cookieCsrf = call.request.cookies[csrfCookie.cookieName]
                ?: return@post call.respond(HttpStatusCode.Unauthorized)
            if (cookieCsrf.length > 500) return@post call.respond(HttpStatusCode.BadRequest)

            val exchangeToken = jwtVerification
                .withIssuer(issuer)
                .withClaim(
                    "csrf", runCatching { cookieCsrf.unHex.sha256 }.getOrNull()
                        ?: return@post call.respond(HttpStatusCode.BadRequest)
                )
                .build()
                .runCatching { verify(confirmation.exchangeToken) }
                .onFailure { return@post call.respond(HttpStatusCode.BadRequest) }
                .getOrElse { error("Unexpected null JWT") }

            transaction { User.find { Users.email eq exchangeToken.getClaim("sub").asString() }.first() }
                .let { user ->
                    if (confirmation.code.filter { it in Auth.validAuthCodeCharSet } != user.authCode) {
                        return@post call.respond(
                            HttpStatusCode.Unauthorized, LoginConfirmationError.AuthCodeInvalid.response)
                    }
                }

            val accessToken = createAccessToken(exchangeToken)

            call.expireCookie(csrfCookie)
            call.addCookie("Access-Token", accessToken, now.plus(8, ChronoUnit.DAYS))

            call.respond(LoginConfirmationResponse())
        }
    }
    authenticate {
        head { call.respond(HttpStatusCode.OK) }
        get("terminate") {
            call.expireCookie(csrfCookie)
            call.expireCookie("Access-Token")
            call.respond(true)
        }
    }
}
