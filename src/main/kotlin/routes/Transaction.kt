package routes

import io.ktor.http.HttpStatusCode
import io.ktor.server.application.call
import io.ktor.server.response.respond
import io.ktor.server.routing.Route
import io.ktor.server.routing.get
import io.ktor.server.routing.put
import io.ktor.server.routing.route
import lib.extensions.receiveAndRespond
import lib.extensions.requirePermissions
import lib.extensions.usePrincipal
import models.NewTransaction
import models.Permission
import models.Transaction
import models.TransactionData
import models.TransactionPosition
import models.Transactions
import models.User
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDateTime

fun Route.transactionRoute() = route("/transactions") {
    get {
        call usePrincipal { principal ->
            call.respond(transaction {
                Transaction.find { Transactions.user eq principal.id }.map { it.data }
            })
        }
    }
    put {
        call.requirePermissions(Permission.ManageTransactions) ?: return@put call.respond(HttpStatusCode.Forbidden)
        call.receiveAndRespond { tx: NewTransaction ->
            call usePrincipal { principal ->
                transaction {
                    addTransactionForUser(principal, tx)
                }
            }
        }
    }
    put("{userId}") {
        call.requirePermissions(Permission.ManageTransactions) ?: return@put call.respond(HttpStatusCode.Forbidden)
        val userId = call.parameters["userId"]?.toIntOrNull() ?: return@put call.respond(HttpStatusCode.BadRequest)
        call.receiveAndRespond { tx: NewTransaction ->
            transaction {
                addTransactionForUser(User.findById(userId) ?: error("user not found"), tx)
            }
        }
    }
}

private fun addTransactionForUser(txUser: User, tx: NewTransaction): TransactionData {
    val createdTx = Transaction.new {
        type = tx.type
        user = txUser.id
        createdAt = LocalDateTime.now()
    }
    tx.positions.forEach { position ->
        TransactionPosition.new {
            description = position.description
            unitPrice = position.unitPrice
            quantity = position.quantity
            transaction = createdTx
        }
    }

    return createdTx.data
}
