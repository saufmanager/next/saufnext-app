package routes

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import lib.extensions.receiveAndRespond
import lib.extensions.requirePermissions
import lib.extensions.usePrincipal
import models.*
import org.jetbrains.exposed.sql.transactions.transaction

fun Route.productsRoute() = route("/products") {
    get {
        call.respond(transaction { Product.all().map { it.data } })
    }
    put {
        call.requirePermissions(Permission.ManageProducts) ?: return@put call.respond(HttpStatusCode.Forbidden)
        call.receiveAndRespond { product: ProductData ->
            transaction {
                Product.new {
                    name = product.name
                    price = product.price
                }.data
            }
        }
    }
    patch {
        call.requirePermissions(Permission.ManageProducts) ?: return@patch call.respond(HttpStatusCode.Forbidden)
        call.receiveAndRespond { product: ProductData ->
            transaction {
                Product[product.id].apply {
                    name = product.name
                    price = product.price
                }.data
            }
        }
    }
    delete("{id}") {
        call.requirePermissions(Permission.ManageProducts) ?: return@delete call.respond(HttpStatusCode.Forbidden)
        val id = call.parameters["id"]?.toIntOrNull() ?: return@delete call.respond(HttpStatusCode.BadRequest)
        transaction { Product[id].delete() }
        call.respond(true)
    }
    get("{id}/buy") {
        val id = call.parameters["id"]?.toIntOrNull() ?: return@get call.respond(HttpStatusCode.BadRequest)
        call usePrincipal { principal ->
            transaction {
                val product = Product.findById(id) ?: return@transaction HttpStatusCode.NotFound to false
                HttpStatusCode.OK to TransactionPosition.new {
                    description = product.name
                    unitPrice = product.price
                    quantity = 1
                    transaction = Transaction.new {
                        type = TransactionType.Purchase
                        user = principal.id
                    }
                }.data
            }.let { call.respond(it.first, it.second) }
        }
    }
}
