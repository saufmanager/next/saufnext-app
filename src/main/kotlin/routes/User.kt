package routes

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import lib.extensions.receiveAndRespond
import lib.extensions.requirePermissions
import lib.extensions.usePrincipal
import models.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.transactions.transaction

fun Route.userRoute() = route("/user") {
    get("me") {
        call usePrincipal { call.respond(transaction { User.findById(it.id)!!.data }) }
    }
    get {
        call.requirePermissions(Permission.ManageUsers) ?: return@get call.respond(HttpStatusCode.Forbidden)
        call.respond(transaction { User.all().map { it.data } })
    }
    patch {
        call.requirePermissions(Permission.ManageUsers) ?: return@patch call.respond(HttpStatusCode.Forbidden)
        call.receiveAndRespond { user: UserData ->
            transaction {
                User[user.id].apply user@{
                    email = user.email
                    UserPermissions.deleteWhere { UserPermissions.user eq user.id }
                    user.permissions.forEach { perm ->
                        UserPermission.new {
                            this.user = this@user.id
                            this.permission = perm
                        }
                    }
                }.data
            }
        }
    }
}
