import kotlin.reflect.KProperty

object Env {
    operator fun get(name: String): String? = System.getenv(name)
}

class EnvDelegate(private val name: String, private val default: () -> String? = { null }) {
    operator fun getValue(self: Any?, prop: KProperty<*>) =
        System.getenv(name) ?: default() ?: error("Env variable $name not defined")
}

class EnvTransformDelegate<T>(
    private val name: String,
    private val transform: (String?) -> T,
    private val default: () -> T? = { null },
) {
    private val value by lazy { transform(System.getenv(name)) ?: default() ?: error("Env variable $name not defined") }
    operator fun getValue(self: Any?, prop: KProperty<*>) = value
}

fun env(name: String, default: () -> String? = { null }) = EnvDelegate(name, default)
inline fun <reified T> transformEnv(name: String, noinline transform: (String) -> T) =
    EnvTransformDelegate(name, { it?.let(transform) })
inline fun <reified T> transformEnv(name: String, noinline default: () -> T?, noinline transform: (String) -> T) =
    EnvTransformDelegate(name, { it?.let(transform) }, default)

