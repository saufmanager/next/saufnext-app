import jakarta.mail.Message
import jakarta.mail.Session
import jakarta.mail.Transport
import jakarta.mail.internet.InternetAddress
import jakarta.mail.internet.MimeMessage
import java.util.*

private val smtpHost = System.getenv("SMTP_HOST") ?: "localhost"
private val smtpPort = System.getenv("SMTP_PORT")?.toInt() ?: if (smtpHost == "localhost") 2028 else 25
private val smtpFrom = System.getenv("SMTP_FROM") ?: "robot@localhost"
private val smtpUser = System.getenv("SMTP_USER")
private val smtpPassword = System.getenv("SMTP_PASSWORD")

data class Email(
    val recipients: List<String>,
    val subject: String,
    val text: String,
) {

    fun send() {
        val props = Properties().apply {
            put("mail.smtp.host", smtpHost)
            put("mail.smtp.port", smtpPort)
        }
        val session = Session.getInstance(props, null)

        val msg = MimeMessage(session).apply {
            setFrom(smtpFrom)
            setRecipients(
                Message.RecipientType.TO,
                recipients.map { InternetAddress(it) }.toTypedArray(),
            )
            subject = subject
            sentDate = Date()
            setText(text)
        }
        smtpUser?.let { smtpUser ->
            Transport.send(msg, smtpUser, smtpPassword)
        } ?: Transport.send(msg)
    }

}