package models

import io.ktor.server.auth.*
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction

enum class Permission {
    ManageProducts, ManageTransactions, ManageUsers,
    ManageCash,
}

object Users : IntIdTable() {
    val email = varchar("email", 80).uniqueIndex()
    val authCode = varchar("auth_code", 16).nullable()
}

object UserPermissions : IntIdTable() {
    val user = reference("user", Users)
    val permission = enumeration("permission", Permission::class)
}

class User(id: EntityID<Int>) : IntEntity(id), Principal {
    companion object : IntEntityClass<User>(Users) {
        fun byEmailOrCreate(email: String, andDo: User.() -> Unit = {}) = transaction {
            User.find { Users.email eq email }.asSequence().ifEmpty {
                sequenceOf(User.new { this.email = email })
            }.first().apply(andDo)
        }
        fun byEmail(email: String) = transaction {
            User.find { Users.email eq email }.first()
        }
    }

    var email by Users.email
    var authCode by Users.authCode

    val transactions by Transaction referrersOn Transactions.user
    val permissions by UserPermission referrersOn UserPermissions.user
    val balance get() = transactions.sumOf { it.calculationAmount }

    val data get() = UserData(
        id.value, transactions.map { it.id.value }, email, balance, permissions.map { it.permission },
    )

    fun hasPermission(permission: Permission) = permissions.any { it.permission == permission }
}

class UserPermission(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<UserPermission>(UserPermissions)

    var user by UserPermissions.user
    var permission by UserPermissions.permission
}

@Serializable
data class UserData(
    val id: Int = 0, val transactionIDs: List<Long>,
    val email: String, val balance: Int,
    val permissions: List<Permission>,
)
