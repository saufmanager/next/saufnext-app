package models

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object Products : IntIdTable() {
    val name = varchar("name", 40).uniqueIndex()
    val price = integer("price")
}

class Product(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Product>(Products)

    var name by Products.name
    var price by Products.price

    val data get() = ProductData(id.value, name, price)
}

@Serializable
data class ProductData(
    val id: Int = 0, val name: String, val price: Int,
) {
    init {
        require(price in -1_000_000..1_000_000) { "price must be in range" }
    }
}
