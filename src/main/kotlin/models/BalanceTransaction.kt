package models

import kotlinx.serialization.Serializable
import models.Transactions.default
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.javatime.datetime
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset

enum class BalanceTxType {
    Incoming, Outgoing,
}

object BalanceTransactions : LongIdTable() {
    val type = enumeration("type", BalanceTxType::class)
    val description = varchar("description", 32).default("–")
    val amount = integer("amount")
    val createdAt = datetime("created_at").default(LocalDateTime.now())
}

class BalanceTransaction(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<BalanceTransaction>(BalanceTransactions)

    var type by BalanceTransactions.type
    var description by BalanceTransactions.description
    var amount by BalanceTransactions.amount
    var createdAt by BalanceTransactions.createdAt

    val calculationAmount get() = when (type) {
        BalanceTxType.Outgoing -> -amount
        BalanceTxType.Incoming -> amount
    }

    val data get() = BalanceTransactionData(
        id.value,
        type,
        amount,
        description,
        createdAt.atZone(ZoneId.systemDefault()).toEpochSecond(),
    )
}

@Serializable
data class BalanceTransactionData(
    val id: Long = 0, val type: BalanceTxType,
    val amount: Int, val description: String, val createdAt: Long
)
