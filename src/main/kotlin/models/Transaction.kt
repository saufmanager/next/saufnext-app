package models

import kotlinx.serialization.Serializable
import models.Product.Companion.referrersOn
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.javatime.datetime
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset

enum class TransactionType {
    Purchase, Correction, TopUp
}

object Transactions : LongIdTable() {
    val user = reference("user", Users)
    val type = enumeration("type", TransactionType::class)
    val createdAt = datetime("created_at").default(LocalDateTime.now())
}

class Transaction(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<Transaction>(Transactions)

    var user by Transactions.user
    var type by Transactions.type
    var createdAt by Transactions.createdAt

    val positions by TransactionPosition referrersOn TransactionPositions.transaction
    val totalAmount get() = positions.sumOf { it.amount }
    val calculationAmount get() = when (type) {
        TransactionType.Purchase -> -totalAmount
        TransactionType.Correction, TransactionType.TopUp -> totalAmount
    }

    val data get() = TransactionData(
        id.value,
        type,
        positions.map { it.id.value },
        totalAmount,
        createdAt.atZone(ZoneId.systemDefault()).toEpochSecond(),
    )
}

@Serializable
data class TransactionData(
    val id: Long = 0, val type: TransactionType,
    val positionIDs: List<Long>, val totalAmount: Int,
    val createdAt: Long,
) {
    init {
        require(positionIDs.all { it > 0 }) { "all position IDs must be > 0" }
    }
}

@Serializable
data class NewTransaction(
    val id: Long = 0, val type: TransactionType,
    val positions: List<NewTransactionPositionData>,
)