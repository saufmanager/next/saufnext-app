package models

import kotlinx.serialization.Serializable
import models.TransactionPositions.references
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.LongIdTable

object TransactionPositions : LongIdTable() {
    val description = varchar("description", 100)
    val unitPrice = integer("price")
    val quantity = integer("quantity")
    val transaction = reference("tx", Transactions)
}

class TransactionPosition(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<TransactionPosition>(TransactionPositions)

    var description by TransactionPositions.description
    var unitPrice by TransactionPositions.unitPrice
    var quantity by TransactionPositions.quantity
    var transaction by Transaction referencedOn TransactionPositions.transaction

    val amount get() = quantity * unitPrice

    val data get() =
        TransactionPositionData(id.value, unitPrice, quantity, description, amount, transaction.id.value)
}

@Serializable
data class TransactionPositionData(
    val id: Long = 0, val unitPrice: Int, val quantity: Int, val description: String,
    val amount: Int,
    val transactionID: Long,
) {
    init {
        // Add some reasonable limitations
        require(unitPrice in -1_000_000 until 1_000_000) { "unit price must be in range" }
        require(quantity in 0 until 1_000) { "quantity must be in range" }
        require(transactionID > 0) { "transaction ID must be > 0" }
    }
}
@Serializable
data class NewTransactionPositionData(
    val unitPrice: Int, val quantity: Int, val description: String,
) {
    init {
        // Add some reasonable limitations
        require(unitPrice in -1_000_000 until 1_000_000) { "unit price must be in range" }
        require(quantity in 0 until 1_000) { "quantity must be in range" }
    }
}
