import lib.extensions.sha256
import java.security.SecureRandom

object Auth {
    val validAuthCodeCharSet = ('A'..'F') + ('0'..'9')

    private val random = SecureRandom()
    private val salt = ByteArray(256 / 8).also { random.nextBytes(it) }
    private val alphabet = ('A'..'Z') + ('a'..'z')

    fun generateIDPart(email: String) =
        (email.toByteArray() + salt).sha256
            .filter { it in alphabet }
            .padEnd(2, alphabet[email.first().code % alphabet.size])
            .substring(0 until 2)
            .uppercase()
    fun generateCode(email: String) =
        "${generateIDPart(email)}${random.nextInt(100_000, 1_000_000)}"
    private fun makeReadable(code: String) =
        code.reversed().windowed(3, 3, true) { it.reversed() }.reversed().joinToString(" ")

    fun sendCodeEmail(email: String) = generateCode(email).also {
        Email(
            recipients = listOf(email),
            subject = "$projectName: Dein Authentifizierungscode",
            text = """
                Dein Authentifizierungscode lautet:
                ${makeReadable(it)}

                Gib diesen Code nicht weiter.
            """.trimIndent()
        ).send()
    }
}