package lib.extensions

import Config
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.plugins.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.util.date.*
import models.Permission
import models.User
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.Instant

suspend inline infix fun <reified T : Any> ApplicationCall.receive(then: (T) -> Unit) = receive<T>().let(then)
suspend inline infix fun <reified T : Any, reified R : Any> ApplicationCall.receiveAndRespond(after: (T) -> R) =
    respond(after(receive()))
inline infix fun ApplicationCall.usePrincipal(block: (User) -> Unit) =
    block(principal() ?: error("no principal"))

fun ApplicationCall.requirePermissions(vararg permissions: Permission): Unit? =
    transaction {
        usePrincipal { principal ->
            return@transaction if (permissions.all { principal.hasPermission(it) }) Unit else null
        }
    }


val ApplicationCall.isSecureTransport get() = request.origin.scheme == "https" || request.origin.scheme == "wss"

fun ApplicationCall.addCookie(
    name: String,
    value: String,
    expires: Instant,
    encoding: CookieEncoding = CookieEncoding.RAW
) = response.cookies.append(
    name = name.cookieName,
    value = value,
    expires = GMTDate(expires.toEpochMilli()),
    path = "/",
    secure = Config.isProduction,
    httpOnly = true,
    encoding = encoding,
)

fun ApplicationCall.expireCookie(name: String) = addCookie(
    name = name,
    value = "",
    expires = Instant.EPOCH
)

val String.cookieName get() = "${if (Config.isProduction) "__Host-" else ""}$this"