package lib.extensions

import java.time.Instant
import java.time.temporal.ChronoUnit

val now: Instant get() = Instant.now()
fun Instant.plusMinutes(amountToAdd: Long): Instant = plus(amountToAdd, ChronoUnit.MINUTES)
