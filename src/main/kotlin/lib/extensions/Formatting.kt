package lib.extensions

val ByteArray.hex get() = joinToString("") { "%02x".format(it) }
val String.unHex get() = chunked(2).map { it.toUByte(16).toByte() }.toByteArray()
