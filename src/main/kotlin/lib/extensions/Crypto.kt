package lib.extensions

import java.security.MessageDigest

val ByteArray.sha256 get() = MessageDigest.getInstance("SHA-256").digest(this).hex
val String.sha256 get() = toByteArray().sha256
