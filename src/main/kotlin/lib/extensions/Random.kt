package lib.extensions

import java.util.*

fun Random.nextBytes(count: Int) = ByteArray(count).also { nextBytes(it) }
fun Random.nextBits(bitCount: Int) = nextBytes(bitCount / 8)
