import mu.KLogger
import mu.KotlinLogging
import java.io.InputStream
import kotlin.reflect.KClass
import kotlin.reflect.KProperty

class DelegatedLogger(name: String) {
    private val logger by lazy { KotlinLogging.logger(name) }

    operator fun getValue(thisRef: Any?, property: KProperty<*>): KLogger {
        return logger
    }
}

class DelegatedClassLogger<THIS : Any>(klass: KClass<THIS>) {
    private val logger by lazy { KotlinLogging.logger(klass.java.canonicalName) }

    operator fun getValue(thisRef: THIS, property: KProperty<*>): KLogger {
        return logger
    }
}

fun namedLogger(name: String) = DelegatedLogger(name)
inline fun <reified THIS : Any> THIS.classLogger() = DelegatedClassLogger(THIS::class)
inline val <reified THIS : Any> THIS.classLogger get() = classLogger()

infix fun InputStream.pipeInto(logger: KLogger) =
    use { bufferedReader().useLines { l -> l.forEach { logger.info("console > $it") } } }

fun Process.consolePipedInto(logger: KLogger) = apply { inputStream pipeInto logger }