package db

import org.jetbrains.exposed.sql.Database

object DB {

    fun connect() {
        Database.connect(
            "jdbc:${System.getenv("DATABASE_URL") ?: "postgresql://localhost/db?user=user&password=pw"}",
            "org.postgresql.Driver",
        )
    }

}