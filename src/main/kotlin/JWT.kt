import com.auth0.jwt.JWT
import com.auth0.jwt.JWTCreator
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.interfaces.DecodedJWT
import com.auth0.jwt.interfaces.JWTVerifier
import com.auth0.jwt.interfaces.Verification
import lib.extensions.now
import lib.extensions.plusMinutes
import lib.extensions.sha256
import models.User
import org.jetbrains.exposed.sql.transactions.transaction
import routes.LoginConfirmation
import routes.LoginRequest
import java.security.KeyPairGenerator
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.time.temporal.ChronoUnit

private val jwtKeypair =
    KeyPairGenerator.getInstance("RSA").apply { initialize(2048) }.genKeyPair()
private val algorithm = Algorithm.RSA256(jwtKeypair.public as RSAPublicKey, jwtKeypair.private as RSAPrivateKey)

fun JWTCreator.Builder.withShortExpiry(): JWTCreator.Builder = withExpiresAt(now.plusMinutes(15))
fun JWTCreator.Builder.withLongExpiry(): JWTCreator.Builder = withExpiresAt(now.plus(7, ChronoUnit.DAYS))

val jwtBase: JWTCreator.Builder
    get() = JWT.create()
        .withAudience("$projectName-users")
        .withIssuer(issuer)

fun LoginRequest.createExchangeToken(csrfCode: ByteArray): String = jwtBase
    .withClaim("sub", email)
    .withClaim("csrf", csrfCode.sha256)
    .withShortExpiry()
    .sign(algorithm)

fun createAccessToken(exchangeToken: DecodedJWT): String = jwtBase
    .withClaim("sub", exchangeToken.getClaim("sub").asString())
    .withArrayClaim("permissions", transaction {
        User.byEmail(exchangeToken.getClaim("sub").asString()).permissions.map { it.permission.name }.toTypedArray()
    })
    .withLongExpiry()
    .sign(algorithm)

val jwtVerification: Verification get() = JWT.require(algorithm).withIssuer(issuer).acceptLeeway(10)

val accessTokenVerifier: JWTVerifier = jwtVerification.build()
