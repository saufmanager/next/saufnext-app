
object Config {
    private val logger by classLogger

    val allowedUserDomains by transformEnv("ALLOWED_USER_DOMAINS", default = { listOf() }) {
        it.split(",")
    }

    fun userDomainAllowed(domain: String) =
        (allowedUserDomains.isEmpty() || domain in allowedUserDomains).also {
            logger.info("Domain not allowed: $domain. Allowed domains: $allowedUserDomains")
        }
    fun userEmailAllowed(email: String) = userDomainAllowed(email.split("@").last())

    val environment by env("ENVIRONMENT") { "production" }
    val isDevelopment get() = environment == "development"
    val isProduction get() = environment == "production"

    val domain by env("DOMAIN") { "localhost" }
}
