import Authorization.setup
import db.DB
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.plugins.cors.routing.*
import io.ktor.server.plugins.forwardedheaders.*
import io.ktor.server.routing.*
import models.*
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import routes.balanceTransactionRoute
import routes.loginRoute
import routes.productsRoute
import routes.transactionRoute
import routes.userRoute

const val projectName = "saufnext"
const val issuer = "$projectName-backend"

private val mainLogger by namedLogger("main")

val tables = arrayOf(
    Products, Transactions, TransactionPositions, Users, UserPermissions,
    BalanceTransactions,
)

fun main() {
    mainLogger.info("Starting $projectName")
    mainLogger.info("Allowed email domains: ${Config.allowedUserDomains}")

    DB.connect()
    transaction {
        SchemaUtils.create(*tables, inBatch = true)
        execInBatch(
            SchemaUtils.addMissingColumnsStatements(*tables, withLogs = true)
        )
    }

    embeddedServer(Netty, port = 8090) {
        install(ContentNegotiation) {
            json()
        }

        install(Authentication) {
            setup()
        }

        if (Config.isProduction) {
            install(XForwardedHeaders)
        }

        install(CORS) {
            if (Config.isDevelopment) {
                anyHost()
            } else {
                allowHost(Config.domain)
                allowSameOrigin = true
            }
            allowMethod(HttpMethod.Options)
            allowMethod(HttpMethod.Put)
            allowMethod(HttpMethod.Patch)
            allowMethod(HttpMethod.Delete)
            listOf(
                "Accept",
                "Accept-Encoding",
                "Connection",
                "Content-Length",
                "Content-Type",
                "Cookie",
                "DNT",
                "Host",
                "Origin",
                "Referer",
                "X-Forwarded-Host",
                "X-Forwarded-Origin",
                "X-Forwarded-For",
                "X-Forwarded-Port",
            ).forEach { allowHeader(it) }
        }

        routing()
    }.start(wait = true)
}

fun Application.routing() = routing {
    loginRoute()
    authenticate {
        productsRoute()
        transactionRoute()
        userRoute()
        balanceTransactionRoute()
    }
}
